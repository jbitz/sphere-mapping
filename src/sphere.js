import * as THREE from 'three';

class Sphere {
    constructor(scene, uvMapping) {
        this.geometry = new THREE.SphereGeometry(.999, 64, 32);
        this.material = new THREE.MeshBasicMaterial({
            color: 0xffff00,
            wireframe: false,
        });
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.scene = scene;
        this.uvMapping = uvMapping;

        this.uvLines = [];
        this.tangentVectors = {u: null, v: null};
        this.point = null;
        //this.scene.add(this.mesh);
    }

    setUVMapping(uvMapping) {
        this.uvMapping = uvMapping;
    }
   
    setTangentVectorLocation(u, v) {
        this.scene.remove(this.tangentVectors.u, this.tangentVectors.v);
        this._setPoint(u, v);
        const origin = this.uvMapping.uvToWorld(u, v);

        const dU = this.uvMapping.getUDerivative(u, v);
        const lengthU = dU.length();
        dU.normalize();
        const arrowU = new THREE.ArrowHelper(dU, origin, lengthU, 0xff0000);
        this.scene.add(arrowU);

        const dV = this.uvMapping.getVDerivative(u, v);
        const lengthV = dV.length();
        dV.normalize();
        const arrowV = new THREE.ArrowHelper(dV, origin, lengthV, 0x00ff00);
        this.scene.add(arrowV);

        this.tangentVectors = {u: arrowU, v: arrowV};
    }

    _setPoint(u, v, materialProps) {
        if (this.point) {
            this.scene.remove(this.point);
        }

        const pos = this.uvMapping.uvToWorld(u, v);
        const geometry = new THREE.SphereGeometry(.05, 16, 8);
        const material = new THREE.MeshBasicMaterial(materialProps);
        const mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(pos.x, pos.y, pos.z);
        this.scene.add(mesh);

        this.point = mesh;
    }

    removeAddedObjects() {
        if (this.point) {
            this.scene.remove(this.point);
        }
        this.point = null;

        this.scene.remove(this.tangentVectors.u, this.tangentVectors.v);
        this.tangentVectors.u = null;
        this.tangentVectors.v = null;

        this.clearUVGrid();
    }

    clearUVGrid() {
        for (const line of this.uvLines) {
            this.scene.remove(line);
        }
        this.uvLines = [];
    }

    drawUVGrid(gridResolution = 10, fineResolution = 10) {
        const lineMaterial = new THREE.LineBasicMaterial({
            color: 0x770099
        });
        const uMin = this.uvMapping.uMin();
        const uMax = this.uvMapping.uMax();
        const vMin = this.uvMapping.vMin();
        const vMax = this.uvMapping.vMax();

        const dU = (uMax - uMin) / gridResolution;
        const dV = (vMax - vMin) / gridResolution;
        // Draw lines of increasing v for each u coordinate
        for (let uStep = 0; uStep <= gridResolution; uStep++) {
            const points = [];
            
            const u = uMin + uStep * dU;
            for (let vStep = 0; vStep <= gridResolution; vStep++) {
                const v = vMin + vStep * dV;
                
                for (let innerStep = 0; innerStep <= fineResolution; innerStep++) {
                    const vFine = v + dV * (innerStep / fineResolution);
                    const pt = this.uvMapping.uvToWorld(u, vFine);
                    // Only add point if it is in domain of uv mapping
                    // uvToWorld returns null if it is not
                    if (pt) points.push(pt)
                }   
            }
            const geometry = new THREE.BufferGeometry().setFromPoints(points);
            const line = new THREE.Line(geometry, lineMaterial);
            this.scene.add(line);
            this.uvLines.push(line);
        }

        // Draw lines of increasing u for each v coordinate
        for (let vStep = 0; vStep <= gridResolution; vStep++) {
            const points = [];
            const v = vMin + vStep * dV;

            for (let uStep = 0; uStep <= gridResolution; uStep++) {
                const u = uMin + uStep * dU;

                for (let innerStep = 0; innerStep <= fineResolution; innerStep++) {
                    const uFine = u + dU * (innerStep / fineResolution);
                    const pt = this.uvMapping.uvToWorld(uFine, v)
                    if (pt) points.push(pt);
                }   
            }
            const geometry = new THREE.BufferGeometry().setFromPoints(points);
            const line = new THREE.Line(geometry, lineMaterial);
            this.scene.add(line);
            this.uvLines.push(line);
        }
    }
}

module.exports = {
    Sphere,
}