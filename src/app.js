import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/Addons';
import { Sphere } from './sphere';
import { EqualVerticalMapping } from './mappings/equalvertical';
import { StereographicMapping } from './mappings/stereographic';
import { CircleStretchMapping } from './mappings/circlestretch';
import { setUpCanvas } from './uvcanvas';


import 'katex/dist/katex.min.css'
import { renderMathInElement } from './katexautorender/auto-render';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, (window.innerWidth / 2) / window.innerHeight, 
    .1, 1000);
    
camera.position.z = 5;

const renderer = new THREE.WebGLRenderer();

renderer.setSize(window.innerWidth / 2, window.innerHeight);
renderer.domElement.style.display = 'inline';
document.querySelector('#canvas-container').appendChild(renderer.domElement);
controls = new OrbitControls(camera, renderer.domElement);


const sphere = new Sphere(scene, EqualVerticalMapping);
setUpCanvas(StereographicMapping, 30);

// Set up UI
(() => {
    const gridLinesSlider = document.querySelector('#grid-resolution');
    gridLinesSlider.addEventListener('input', () => {
        sphere.clearUVGrid();
        sphere.drawUVGrid(gridLinesSlider.value, 20);
    })

    // Radio buttons to change projection modes
    const projectionButtons = document.querySelectorAll('input[name="projection-type"]');
    const setProjection = target => {
        mapping = {
            'equalvertical': EqualVerticalMapping,
            'circlestretch': CircleStretchMapping,
            'stereographic': StereographicMapping,
        }[target.value];
        sphere.setUVMapping(mapping);

        const desc = document.querySelector('#mapping-description')
        desc.innerHTML = mapping.getDescription();
        renderMathInElement(desc, {
            delimiters: [
                {left: '$$', right: '$$', display: true},
                {left: '$', right: '$', display: false},
                {left: '\\(', right: '\\)', display: false},
                {left: '\\[', right: '\\]', display: true}
            ]
        });

        sphere.removeAddedObjects();
        sphere.drawUVGrid(gridLinesSlider.value, 20);

        //sphere.setTangentVectorLocation(5, 5);
    }
    for (const button of projectionButtons) {
        button.addEventListener('change', e => setProjection(e.target));
    }
    setProjection(document.querySelector('input[name="projection-type"]:checked'));

   
    
})();

// Set up animation loop for three.js
function animate() {
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
}
animate();