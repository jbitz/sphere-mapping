let canvas = document.querySelector('#viewport-2d');
let ctx = canvas.getContext('2d');
let uvMapping = null;

let dotX = canvas.width / 2;
let dotY = canvas.width / 2;

function drawUVLines(gridResolution) {
    ctx.strokeStyle = '#770099';
    ctx.lineWidth = 2;
    for (let i = 0; i <= gridResolution; i++) {
        const x = i * (canvas.width / gridResolution);
        ctx.beginPath();
        ctx.moveTo(x, 0);
        ctx.lineTo(x, canvas.height);
        ctx.stroke();

        const y = i * (canvas.height / gridResolution);
        ctx.beginPath();
        ctx.moveTo(0, y);
        ctx.lineTo(canvas.width, y);
        ctx.stroke();
    }
}

function canvasToUV(x, y) {
    const uMin = uvMapping.uMin();
    const uMax = uvMapping.uMax();
    const vMin = uvMapping.vMin();
    const vMax = uvMapping.vMax();
}

export function setUpCanvas(mapping, gridResolution=10) {
    uvMapping = mapping;
    drawUVLines(gridResolution);
}