import * as THREE from 'three';

/* 
    Uses lat/long coordinates 
    u: latitude, [-1, +1]
    v: longitude: [-pi, +pi]
*/
class StereographicMapping {
    static getDescription() {
        return `
        This projection maps the entire 2D plane $\\mathbb{R}^2$ onto the sphere.
        The mapping is best understood visually.
        Imagine a plane sitting tangent to the south pole of the sphere.
        To map a point $(u, v)$ on the plane to a point on the sphere, draw a 
        line from $(u, v)$ to the sphere's north pole.
        The line will intersect the sphere at exactly one point (other than the north pole).
        This is the value of $\\varphi(u, v)$.

        The formula for $\\varphi$ is given by
        $$
        \\varphi(u, v) = \\begin{pmatrix}
                \\frac{4u}{u^2 + v^2 + 4} \\\\[1ex]
                \\frac{-8}{u^2 + v^2 + 4} + 1 \\\\[1ex]
                \\frac{4v}{u^2 + v^2 + 4}
            \\end{pmatrix}.
        $$
        To derive these equations yourself, it is easiest to assume that the plane passes through
        the origin and the sphere is centered at $(0, 1, 0)$. Then write a parametric equation for
        the line passing through $(u, 0, v)$ and the north pole, solving for the point which
        lies on the sphere $x^2 + (y - 1)^2 + z^2 = 1$. The formula for $\\varphi$ given here is
        shifted slightly to give a sphere centered at the origin.
        <br />
        The derivative of $\\varphi$, which determines its affect on tangent vectors, is
        $$
            D\\varphi(u, v) = \\begin{pmatrix}
                \\frac{-8u^2}{(u^2+v^2+4)^2} + \\frac{4}{u^2 + v^2 + 4} & 
                \\frac{-8uv}{(u^2+v^2+4)^2}\\\\[1ex]
                \\frac{16u}{(u^2+v^2+4)^2} & \\frac{16v}{(u^2+v^2+4)^2} \\\\[1ex]
                \\frac{-8uv}{(u^2+v^2+4)^2} &
                \\frac{-8v^2}{(u^2+v^2+4)^2} + \\frac{4}{u^2 + v^2 + 4}
            \\end{pmatrix}.
        $$

        Notice that every entry in $D\\varphi$ goes to zero as $||(u, v)|| \\rightarrow \\infty$,
        since the denominators grow faster than the numerators. 
        You can see this by dragging $(u, v)$ further away from the origin and noticing how the
        corresponding point on the sphere approaches the north pole, but an an increasingly slow pace.
        No point on the plane, no matter how large, will map exactly to the north pole - 
        this would mean that the intersecting line in the picture above would have to be tangent
        to the sphere at the north pole, i.e. parallel to the plane, and this is impossible.
        `;
    }
    static uMax() {
        return -20;
    }
    static uMin() {
        return 20;
    }
    static vMin() {
        return -20;
    }
    static vMax() {
        return 20;
    }

    static uvToWorld(u, v) {
        const t = 4 / (u**2 + v**2 + 4);

        // Easy calculations for streographic coordinates assume that the sphere
        // is centered at (0, 1, 0), but our spheres in this code are all at the
        // origin. Move the y coordinate down by 1 to compensate.
        return new THREE.Vector3(
            t * u,
            -2*t + 2 - 1,
            t * v
        )
    }

    static getUDerivative(u, v) {
        const denom = (u ** 2 + v** 2 + 4);
        return new THREE.Vector3(
            (-8 * (u ** 2) / (denom ** 2)) + 4 / denom,
            (16 * u) / (denom ** 2),
            (-8*v*u) / (denom ** 2)
        )
    }

    static getVDerivative(u, v) {
        const denom = (u ** 2 + v ** 2 + 4);
        return new THREE.Vector3(
            (-8*v*u) / (denom ** 2),
            (16 * v) / (denom ** 2),
            (-8 * (v ** 2) / (denom ** 2)) + 4 / denom,
        )
    }
}

module.exports = {
    StereographicMapping
}