import * as THREE from 'three';

class CircleStretchMapping {
    static getDescription() {
        return `
            <b>TODO: You still need to implement the code for the derivatives, ya dingus! </b>
        `;
    }
    static uMax() {
        return Math.sqrt(Math.PI);
    }
    static uMin() {
        return -Math.sqrt(Math.PI);
    }
    static vMin() {
        return -Math.sqrt(Math.PI);
    }
    static vMax() {
        return Math.sqrt(Math.PI);
    }

    static uvToWorld(u, v) {
        if (u == 0 && v == 0) {
            return new THREE.Vector3(0, 1, 0);
        }

        const norm_sq = u**2 + v**2;
        if (norm_sq > Math.PI) return null;
        if (Math.abs(norm_sq - Math.PI) < .001) return new THREE.Vector3(0, -1, 0);

        const norm = Math.sqrt(norm_sq);
    
        return new THREE.Vector3(
            Math.cos(Math.PI / 2 - norm_sq) * u / norm,
            Math.sin(Math.PI / 2 - norm_sq),
            Math.cos(Math.PI / 2 - norm_sq) * v / norm
        )
    }

    /*
    TODO: Implement derivatives of these maps
    */
    static getUDerivative(u, v) {
        const denom = Math.sqrt(1 - u * u)
        return new THREE.Vector3(
            -1 * Math.sin(v) * u / denom,
            1,
            -1 * Math.cos(v) * u / denom
        )
    }

    static getVDerivative(u, v) {
        const inner_rad = Math.sqrt(1 - u * u);
        return new THREE.Vector3(
            Math.cos(v) * inner_rad,
            0,
            -1 * Math.sin(v) * inner_rad,
        )
    }
}

module.exports = {
    CircleStretchMapping
}