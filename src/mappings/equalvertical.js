import * as THREE from 'three';

/* 
    Uses lat/long coordinates 
    u: latitude, [-1, +1]
    v: longitude: [-pi, +pi]
*/
class EqualVerticalMapping {
    static getDescription() {
        return `
            <b>TODO: Re-evaluate. There's something funky with the $x$ vs $z$-axes, and one of the derivatives shouldn't be negative.</b>
            <br />
            This projection maps the region $[-1, 1] \\times [-\\pi, \\pi)$ onto the sphere.
            The $u$ coordinate becomes the $y$ (vertical) coordinate on the sphere.
            The $v$ coordinate describes an angle from the positive $x$-axis. The map is given by
            $$
            \\varphi(u, v) = \\begin{pmatrix}
                \\sin (v) \\sqrt{1 - u^2} \\\\
                u \\\\
                \\cos (v) \\sqrt{1 - u^2}
            \\end{pmatrix}.
            $$
            Notice that the vertical spacing between each "latitude" line in the picture is the same.
            This is slightly different than in a true latitude-longitude projection, where the 
            lines of latitude are spaced by their <i>angle</i> to the origin, not their vertical distance.
            <br />
            The derivative of $\\varphi$, which determines its affect on tangent vectors, is
            $$
            D\\varphi(u, v) = \\begin{pmatrix}
                \\frac{-u \\sin (v)}{\\sqrt{1 - u^2}} & \\cos (v) \\sqrt{1 - u^2} \\\\[1ex]
                1 & 0 \\\\[1ex]
                \\frac{-u \\cos (v)}{\\sqrt{1 - u^2}} & -\\sin (v) \\sqrt{1 - u^2}
            \\end{pmatrix}.
            $$
            This can also be seen as a kind of "cylindrical" projection.
            Imagine a cylinder of radius $1$ and height of $2$, so that the sphere is circumscribed
            inside it.
            The $u$ coordinate describes the vertical position on the cylinder, and the $v$ coordinate
            describes the angle from the $x$-axis.
            To map a point on the cylinder to one on the sphere, imagine "pushing it in" horizontally
            toward the vertical axis.
            This projection fails to be one-to-one at the north and south poles, where
            every point on the cylinder maps to the same point on the sphere.
            This corresponds to the fact that $\\varphi(1, v) = (0, 1, 0)$ for all values of $v$.
            Likewise, $\\varphi(-1, v) = (0, -1, 0)$ for all values of $v$.
        `;
    }
    static uMax() {
        return 1;
    }
    static uMin() {
        return -1;
    }
    static vMin() {
        return -Math.PI;
    }
    static vMax() {
        return Math.PI;
    }

    static uvToWorld(u, v) {
        if (u < -1 || u > 1) {
            return null;
        }

        const inner_rad = Math.sqrt(1 - u * u);
        return new THREE.Vector3(
            Math.sin(v) * inner_rad,
            u,
            Math.cos(v) * inner_rad
        )
    }

    static getUDerivative(u, v) {
        const denom = Math.sqrt(1 - u * u)
        return new THREE.Vector3(
            -1 * Math.sin(v) * u / denom,
            1,
            -1 * Math.cos(v) * u / denom
        )
    }

    static getVDerivative(u, v) {
        const inner_rad = Math.sqrt(1 - u * u);
        return new THREE.Vector3(
            Math.cos(v) * inner_rad,
            0,
            -1 * Math.sin(v) * inner_rad,
        )
    }
}

module.exports = {
    EqualVerticalMapping
}